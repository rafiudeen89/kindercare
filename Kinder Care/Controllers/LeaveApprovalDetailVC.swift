//
//  LeaveApprovalDetailVC.swift
//  Kinder Care
//
//  Created by Ragavi Rajendran on 17/12/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit

protocol refreshLeaveApprovalDelegate {
  func refreshLeaveApproval()
}


class LeaveApprovalDetailVC: BaseViewController {
  
  
  
  
  
  @IBOutlet weak var contactLabel: UILabel!
  @IBOutlet weak var leaveReasonLabel: UILabel!
  @IBOutlet weak var leaveTypeLabel: UILabel!
  @IBOutlet weak var leaveTillLabel: UILabel!
  @IBOutlet weak var leaveFromLabel: UILabel!
  @IBOutlet weak var leaveDaysLabel: UILabel!
  @IBOutlet weak var requestDateLabel: UILabel!
  @IBOutlet weak var classLabel: UILabel!
  @IBOutlet weak var stuNameLabel: UILabel!
  
  @IBOutlet weak var classValueView: UIView!
  @IBOutlet weak var classView: UIView!
  
  @IBOutlet weak var bottomAcceptRejectViw: CTView!
  
  
  var leaveApprovalArray: LeaveApprovalList?
  var schoolID : Int?
  
  var day: String = ""
  var year: String = ""
  var month: String = ""
  var hours:String = ""
  var mints:String = ""
  var AMPM:String = ""
  var userID: Int?
  var delegate:refreshLeaveApprovalDelegate?
  
  lazy var viewModel : LeaveApprovalViewModel   =  {
    return LeaveApprovalViewModel()
  }()
  
  var selectedUserType :    UserType = .teacher
  var totalDays:Int?
  
  override func viewDidLoad() {
    titleString = "LEAVE APPROVAL"
    super.viewDidLoad()
    
    getLeaveDetails()
    self.viewModel.delegate = self
    self.statusButton()
    // Do any additional setup after loading the view.
  }
  
  func getYearMonth(date : String){
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd"
    let convertedDate = dateFormatter.date(from: date)
    
    dateFormatter.dateFormat = "yyyy"
    if let date1 = convertedDate {
      year  =  dateFormatter.string(from: date1)
      dateFormatter.dateFormat = "MM"
      month = dateFormatter.string(from: date1)
      dateFormatter.dateFormat = "dd"
      day = dateFormatter.string(from: date1)
    }
  }
  
  func getRequestDate(date : String){
    let dateFormatter = DateFormatter()
    //"10 Feb 2020 00:00 AM"
    dateFormatter.dateFormat = "dd MMMM yyyy HH:mm a"
    let convertedDate = dateFormatter.date(from: date)
    
    dateFormatter.dateFormat = "yyyy"
    if let date1 = convertedDate {
      year  =  dateFormatter.string(from: date1)
      dateFormatter.dateFormat = "MM"
      month = dateFormatter.string(from: date1)
      dateFormatter.dateFormat = "dd"
      day = dateFormatter.string(from: date1)
    }
  }
  
  func getLeaveDetails(){
    
    if let name = leaveApprovalArray?.name {
      stuNameLabel.text = name
    }
    if let user_id = leaveApprovalArray?.id {
      userID = user_id
    }
    if let startDate = leaveApprovalArray?.fromDate {
      getYearMonth(date: startDate)
      leaveFromLabel.text = day + "-" + month + "-" + year
    }
    if let endDate = leaveApprovalArray?.toDate {
      getYearMonth(date: endDate)
      leaveTillLabel.text = day + "-" + month + "-" + year
    }
    if let leaveType = leaveApprovalArray?.leaveType {
      leaveTypeLabel.text = leaveType
    }
    if let totDays = leaveApprovalArray?.totDays {
      leaveDaysLabel.text = String(totDays)
    }
    //    if let requestDate = leaveApprovalArray?.requestedDate {
    //      getRequestDate(date: requestDate)
    //      requestDateLabel.text = day + "" + month + "" + year + "" + hours + "" + mints
    //    }
    if let date = leaveApprovalArray?.requestedDate {
      
      requestDateLabel.text = date.getDateAsStringWith(inFormat: "hh:mm a dd MMM yyyy", outFormat: "hh:mm a dd-MM-yyyy ")
    }
    else {
      requestDateLabel.text = ""
    }
    if let reason = leaveApprovalArray?.reason {
      leaveReasonLabel.text = reason
    }
    // contactLabel.text = ""
    if let contact = leaveApprovalArray?.contactNo {
      contactLabel.text = contact
      // contactLabel.text = ""
    }
    if let stuclass = leaveApprovalArray?.stuClass{
      classView.isHidden = false
      classValueView.isHidden = false
      classLabel.text = stuclass
    }
    else
    {
      classView.isHidden = true
      classValueView.isHidden = true
    }
    if let section = leaveApprovalArray?.section{
      if let text = classLabel.text {
        classView.isHidden = false
        classValueView.isHidden = false
        classLabel.text = text + " " + section
      }
    }
    else
    {
      classView.isHidden = true
      classValueView.isHidden = true
    }
  }
  
  func  statusButton(){
    
    let _status = leaveRequestStatus(rawValue:(leaveApprovalArray?.status)!)
    switch  _status {
      
    case .pending:
      
      bottomAcceptRejectViw.isHidden = false
      break
    case .approved:
      
      bottomAcceptRejectViw.isHidden = false
      break
    case .rejected:
      
      bottomAcceptRejectViw.isHidden = true
      break
      
    case .none:
      bottomAcceptRejectViw.isHidden = false
    }
  }
  
  @IBAction func approveBtnAction(_ sender: Any) {
    //    if let schoolID = self.schoolID,let user_id = self.userID {
    //
    //      viewModel.updateLeaveApproval(userID: String(user_id), status: 2, school_id: schoolID)
    //    }
    //      if let schoolID = self.schoolID,let user_id = self.userID {
    //        self.viewModel.updateLeaveApproval(userID:String(user_id) , status: 2, school_id: schoolID)
    //      }
    
    
    
    
    //   self.totalDays = leaveApprovalViewArray[sender.tag].totDays
    if  let userType = UserManager.shared.currentUser?.userType {
      if let _type = UserType(rawValue:userType ) {
        
        switch  _type  {
          
        case .parent,.all,.student: break
          
//        case .teacher:
        
          // self.userID = leaveApprovalViewArray[sender.tag].id
//          if let schoolID = self.schoolID,let user_id = self.userID {
//            viewModel.updateLeaveApproval(userID: String(user_id), status: 2, school_id: schoolID)
//          }
          
        case .admin, .teacher:
          
          let multiStudent =  "\(UserManager.shared.currentUser?.permissions?.multiLevelStudentLeaveDays)"
          let total = "\(totalDays)"
          
          if selectedUserType.stringValue ==  "Student" {
            
            if UserManager.shared.currentUser?.permissions?.studentLeaveApprove == 0 {
              
              displayServerError(withMessage: "You have no access to approve the leave")
              
            }else if total <=  multiStudent {
              
              //              guard let type = UserTypeTitle(rawValue: segmentView.selectedSegmentTitle), let userType = UserType(rawValue: type.id) else {
              //                return
              //              }
              //
              //              selectedUserType = userType
              //              self.userID = leaveApprovalViewArray[sender.tag].id
              
              if let schoolID = self.schoolID,let user_id = self.userID {
                viewModel.updateLeaveApproval(userID: String(user_id), status: 2, school_id: schoolID)
              }
              
            }else {
              displayServerError(withMessage: "You have no access to approve the leave")
            }
            
          }else {
            if total <=  multiStudent {
              
              //              guard let type = UserTypeTitle(rawValue: segmentView.selectedSegmentTitle), let userType = UserType(rawValue: type.id) else {
              //                return
              //              }
              //              selectedUserType = userType
              
              if UserManager.shared.currentUser?.permissions?.studentLeaveApprove == 0 {
                displayServerError(withMessage: "You have no access to approve the leave ")
                return
              }else if UserManager.shared.currentUser?.permissions?.studentLeaveApprove == 1{
                // self.userID = leaveApprovalViewArray[sender.tag].id
                if let schoolID = self.schoolID,let user_id = self.userID {
                  viewModel.updateLeaveApproval(userID: String(user_id), status: 2, school_id: schoolID)
                  
                }else {
                    displayServerError(withMessage: "You have no access to approve the leave")
                }
              }else {
                  displayServerError(withMessage: "You have no access to approve the leave")
              }
            }
          }
          
        case .superadmin:
          //  displayServerError(withMessage: "Super Admin Have No Rights to Approve / Reject")
          //          guard let type = UserTypeTitle(rawValue: segmentView.selectedSegmentTitle), let userType = UserType(rawValue: type.id) else {
          //            return
          //          }
          //
          //          selectedUserType = userType
          // self.userID = leaveApprovalViewArray[sender.tag].id
          
          if let schoolID = self.schoolID,let user_id = self.userID {
            viewModel.updateLeaveApproval(userID: String(user_id), status: 2, school_id: schoolID)
          }
          
        }
      }
    }
  }
  
  
  
  @IBAction func rejectBtnAction(_ sender: Any) {
    if  let userType = UserManager.shared.currentUser?.userType {
      if let _type = UserType(rawValue:userType ) {
        
        switch  _type  {
          
        case .teacher:
          selectedUserType = .teacher
          
          let alert = UIAlertController(title: "Alert", message: "Are you sure you want to Reject ?", preferredStyle: UIAlertController.Style.actionSheet)
          
          alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            action in
          }))
          
          alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: {
            action in
            
            //   self.userID = self.leaveApprovalViewArray[sender.tag].id
            if let schoolID = self.schoolID,let user_id = self.userID {
              self.viewModel.updateLeaveApproval(userID:String(user_id) , status: 3, school_id: schoolID)
            }
            
          }))
          self.present(alert, animated: true, completion: nil)
        case .superadmin:
          //          guard let type = UserTypeTitle(rawValue: segmentView.selectedSegmentTitle), let userType = UserType(rawValue: type.id) else {
          //            return
          //          }
          //          selectedUserType = userType
          
          let alert = UIAlertController(title: "Alert", message: "Are you sure you want to Reject ?", preferredStyle: UIAlertController.Style.actionSheet)
          
          alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            action in
          }))
          
          alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: {
            action in
            
            // self.userID = self.leaveApprovalViewArray[sender.tag].id
            if let schoolID = self.schoolID,let user_id = self.userID {
              self.viewModel.updateLeaveApproval(userID:String(user_id) , status: 3, school_id: schoolID)
            }
            
          }))
          self.present(alert, animated: true, completion: nil)
          
        case .admin:
          
          let multiStudent =  "\(UserManager.shared.currentUser?.permissions?.multiLevelStudentLeaveDays)"
          let total = "\(totalDays)"
          
          //          guard let type = UserTypeTitle(rawValue: segmentView.selectedSegmentTitle), let userType = UserType(rawValue: type.id) else {
          //            return
          //          }
          //   selectedUserType = userType
          
          if UserManager.shared.currentUser?.permissions?.studentLeaveApprove == 0 {
            displayServerError(withMessage: "You have no access to Reject the leave ")
            return
          }
          
          let alert = UIAlertController(title: "Alert", message: "Are you sure you want to Reject ?", preferredStyle: UIAlertController.Style.actionSheet)
          
          alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            action in
          }))
          
          alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: {
            action in
            
            //  self.userID = self.leaveApprovalViewArray[sender.tag].id
            if let schoolID = self.schoolID,let user_id = self.userID {
              self.viewModel.updateLeaveApproval(userID:String(user_id) , status: 3, school_id: schoolID)
            }
            
          }))
          self.present(alert, animated: true, completion: nil)
          
        case .all,.student,.parent: break
          
        }}}
  }
  
}

extension LeaveApprovalDetailVC: leaveApprovalDelegate {
  
  func getLeaveSummaryData(_ leaveSummaryList: [LeaveSummaryDatum]) {
  }
  
  func getLeaveApprovalSuccess(leaveData: LeaveApprovalModel) {
  }
  
  func updateLeaveApprovalSuccess(message:String) {
    displayServerSuccess(withMessage: message)
   // self.delegate?.refreshLeaveApproval()
     
    self.navigationController?.popViewController(animated: true)
  }
  
  func failure(message: String) {
    displayServerError(withMessage: message)
  }
}
