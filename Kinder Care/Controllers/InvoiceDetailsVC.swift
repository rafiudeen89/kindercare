//
//  InvoiceDetailsVC.swift
//  Kinder Care
//
//  Created by CIPL0023 on 19/11/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit
import Razorpay

class InvoiceDetailsVC: BaseViewController, RazorpayPaymentCompletionProtocol {

   //MARK:- Initialization
    var paymentListArray = [PaymentListData]()
    @IBOutlet weak var labelInvoiceDate: UILabel!
    
    @IBOutlet weak var btnPamentOrdownload: UIButton!
    @IBOutlet weak var labelInvoiceID: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelFees: UILabel!
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var labelTerm: UILabel!
    @IBOutlet weak var labelTermType: UILabel!
    @IBOutlet weak var labelFeesType: UILabel!
    var indexPath = Int()
    var childNameArray = [ChildName]()
    var childNameID:Int?
    var term : String = ""
    
    lazy var viewModel : familyInformationViewModel = {
        return familyInformationViewModel()
    }()
   var razorpay : RazorpayCheckout!
    //MARK:- View Life Cycle
       override func viewDidLoad() {
           super.viewDidLoad()
           viewModel.delegate = self
           
           print(paymentListArray,"paymentList Array")
           labelInvoiceID.text =  "#" + paymentListArray[indexPath].studentInvoiceID
           labelInvoiceDate.text = paymentListArray[indexPath].date
           labelTotalAmount.text = "\(paymentListArray[indexPath].totalAmount)"
           var paymentTypevalue = paymentListArray[indexPath].paymentType
           if paymentTypevalue == 1 {
               labelTermType.text = "Full"
           }else{
               labelTermType.text = "Partially"
               
           
           }
           //labelTermType.text  = "Full"
           labelFeesType.text = paymentListArray[indexPath].invoiceName
           let pendingValue =   paymentListArray[indexPath].pending
          // var main_string = ""
           if Int(pendingValue ?? "") == 0 {
               // main_string = "Paid"
               labelStatus.text = "Paid"
               labelStatus.textColor = .green
               titleString = "INVOICE DETAIL"
               btnPamentOrdownload.setTitle("DOWNLOAD INVOICE", for: .normal)
           }else{
             //  main_string = "Pending \(paymentListArray[indexPath].totalAmount)"
               labelStatus.text = "Pending"
               labelStatus.textColor = .ctYellow
               titleString = "PAYMENT"
               btnPamentOrdownload.setTitle("MAKE TO PAY", for: .normal)
           }
          
           
           labelTerm.text = term
           let fees = Float(paymentListArray[indexPath].totalAmount)
           labelFees.text = "\(fees)"
          
          //razorpay = RazorpayCheckout.initWithKey("rzp_test_AX63OOgp8fZhXN", andDelegate: self)
         razorpay = RazorpayCheckout.initWithKey("rzp_live_ZIh4zQdhzAxT8l", andDelegate: self)
          
           if let childName = UserManager.shared.childNameList {
             childNameArray = childName
             self.childNameID = childNameArray.map({$0.id}).first
           }
           // Do any additional setup after loading the view.
       }
       
       //MARK:- Local Methods
       
       
       //MARK:- Button Action
    @IBAction func makePaymentAction(_ sender: UIButton) {
     //  self.navigationController?.popViewController(animated: true)
        let alert = UIAlertController(title: "Info!", message: "Are you sure want to make the payment?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {_ in
            self.showPaymentform()
        }))
        self.present(alert, animated: true, completion: nil)
      // self.viewModel.paymentUpdateSuccessList(studentId: 93, student_invoice_id: "INV0092", transaction_id: "pay_IT1vxAsOKYQEdv", type: 0, received: 3707.33, name: "shakib", terms: 1, fees_type: 0, email: "kcmparent@yopmail.com")
      //  self.viewModel.delegate?.paymentUpdateSuccess()
       // self.showPaymentform()
    }

    internal func showPaymentform(){
        
        let paymentValue = paymentListArray[indexPath].totalAmount * 100
        let option: [String:Any] = [
            "amount" : "100", //100 Amount Value = 1 Rs
            "currency": "INR",
            "description": "School Fees",
            "image":"https://avatars.githubusercontent.com/u/7713209?s=200&v=4",
            "name" : "nisha",
            "prefill": [
                "select_partial": true,
                "contact":"9326300530", // your RazorPay Account Number
                "email":"priyasuri15@gmail.com" // RazorPay EmailId
            ],
            "customer": [
                "name": "Priya",
                "contact": "+917395974109",
                "email": "priyasuri15@gmail.com"
              ],
            "theam" : [
                "color": "#528FF0"
            ]

        ]
        razorpay.open(option)
    }

    func onPaymentError(_ code: Int32, description str: String) {
        let alert = UIAlertController(title: "Failure", message: str, preferredStyle: UIAlertController.Style.alert)

             // add an action (button)
             alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))

             // show the alert
             self.present(alert, animated: true, completion: nil)

    }

    func onPaymentSuccess(_ payment_id: String) {
        let alert = UIAlertController(title: "success", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
            print(payment_id,"paymentid")
             // add an action (button)
        print(payment_id, "Paymentid")
        
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: {_ in
            self.viewModel.paymentUpdateSuccessList(studentId: self.childNameID ?? 0, student_invoice_id: self.paymentListArray[self.indexPath].studentInvoiceID, transaction_id: payment_id, type: 0, received: 1, name: "bala", terms: 1, fees_type: 0, email: "kcmparent@yopmail.com")
        }))

             // show the alert
             self.present(alert, animated: true, completion: nil)

    }
}

extension InvoiceDetailsVC : familyInformationDelegate{
    func familyDetailsList(familyDetails: FamilyInformationData?) {
        
    }
    
    func editPickupSuccess() {
        
    }
    
    func paymentList(paymentList: [PaymentListData]) {
        
    }
    
  
    
    func failure(message: String) {
        print(message)
        labelStatus.text = "Pending"
        labelStatus.textColor = .ctYellow
    }
    
   
    
    func paymentUpdateSuccess() {
        
        labelStatus.text = "Paid"
        labelStatus.textColor = .green
    }
    
    
}
