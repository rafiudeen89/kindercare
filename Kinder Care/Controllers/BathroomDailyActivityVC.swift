//
//  BathroomDailyActivityVC.swift
//  Kinder Care
//
//  Created by Ragavi Rajendran on 10/12/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit
protocol refreshDailyActivityDelegate{
  func refreshDailyActivity()
}

class BathroomDailyActivityVC: BaseViewController {
  
  @IBOutlet weak var cancelAndSendView: CTView!
  @IBOutlet weak var cancelBtn: CTButton!
  @IBOutlet weak var sendBtn: CTButton!
  @IBOutlet weak var tableviewBottomConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var activityTableView: UITableView!{
    didSet {
      activityTableView.register(UINib(nibName: "ActivitySelectedStudentTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivitySelectedStudentTableViewCell")
      activityTableView.register(UINib(nibName: "ActivityPreviewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ActivityPreviewHeaderView")
      activityTableView.register(UINib(nibName: "SelectedStudentsTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectedStudentsTableViewCell")
      activityTableView.register(UINib(nibName: "ActivityDetailsTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivityDetailsTimeTableViewCell")
      activityTableView.register(UINib(nibName: "DescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionTableViewCell")
    }
  }
  
  public var activityId: Int = 0
  var state: Int?
  var activityType: ActivityType!
  
  var viewDailyActivityDetails: DailyActivityDetail?
  var addBathRoomRequest: AddBathRoomActivityRequest?
  var updateBathRoomRequest: UpdateDailyActivityRequest?
  
  var bathRoomType = ""
  var delegate:refreshDailyActivityDelegate?
  
  lazy var viewModel: DailyActivityViewModel   =  {
    return DailyActivityViewModel()
  }()
  
  //MARK: ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    viewModel.delegate = self
    configureUI()
  }
  
  //MARK: configureUI
  func configureUI() {
    
    if viewDailyActivityDetails != nil {
      titleString = "PREVIEW"
      
    }else {
      if activityId != 0 {
        titleString = (activityType.rawValue).uppercased()
        viewModel.viewDailyActivity(activity_id: activityId)
      }else {
        titleString = "PREVIEW"
      }
    }
    
    if  let userType = UserManager.shared.currentUser?.userType {
      
      if let _type = UserType(rawValue:userType) {
        switch _type {
          
        case .admin :
          if activityId != 0 {
            
            let editTopBtn = UIButton(frame: CGRect(x: self.view.frame.width - (16 + 65), y: 15 + safeAreaHeight, width: 70, height: 30))
            editTopBtn.setTitle("Edit", for: .normal)
            editTopBtn.setImage(UIImage(named: "EditWhite"), for: .normal)
            editTopBtn.backgroundColor = UIColor.clear
            editTopBtn.tag = 3
            editTopBtn.addTarget(self, action: #selector(editBtnAction(button:)), for: .touchUpInside)
            self.view.addSubview(editTopBtn)
            
            sendBtn.setTitle("APPROVE", for: .normal)
            cancelBtn.setTitle("REJECT", for: .normal)
            
            switch state {
              
            case 0,1:
              cancelAndSendView.isHidden = true
              tableviewBottomConstraint.constant = -40
              
            default: cancelAndSendView.isHidden = false
              
            }
            
          }else{
            sendBtn.setTitle("Send", for: .normal)
            cancelBtn.setTitle("Cancel", for: .normal)
            
            if state == 1 {
              cancelAndSendView.isHidden = true
            }
            else if state == 0 {
              cancelAndSendView.isHidden = true
            }else{
              cancelAndSendView.isHidden = false
            }
          }
          
        case .teacher:
          if activityId != 0 {
            sendBtn.isHidden = true
            cancelBtn.isHidden = true
            
          }else {
            sendBtn.isHidden = false
            cancelBtn.isHidden = false
          }
          
          sendBtn.setTitle("Send", for: .normal)
          cancelBtn.setTitle("Cancel", for: .normal)
          
          if state == 1 {
            cancelAndSendView.isHidden = true
            
          }else if state == 0 {
            cancelAndSendView.isHidden = true
            
          }else{
            cancelAndSendView.isHidden = false
          }
          
        default :
          cancelBtn.setTitle("Cancel", for: .normal)
          break
        }
      }
    }
  }
  //MARK:- Button Action
  @objc func editBtnAction(button : UIButton) {
    
    if button.tag == 1 {
      let vc = UIStoryboard.AddActivityStoryboard().instantiateViewController(withIdentifier:"StudentListVC") as! StudentListVC
      vc.modalPresentationStyle = .overCurrentContext
      self.navigationController?.present(vc, animated: true, completion: nil)
      
    }else if button.tag == 3 {
      
      let story = UIStoryboard(name: "AddActivity", bundle: nil)
      let nextVC = story.instantiateViewController(withIdentifier: "SelectStudentVC") as! SelectStudentVC
      nextVC.activityType = activityType
      nextVC.viewDailyActivityDetails = viewDailyActivityDetails
      self.navigationController?.pushViewController(nextVC, animated: true)
      
    }else {
      let vc = UIStoryboard.AddActivityStoryboard().instantiateViewController(withIdentifier:"AddActivityVC") as! AddActivityVC
      vc.activityType = activityType
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  
  
  @IBAction func cancelBtnAction(_ sender: Any) {
    if let activityID = viewDailyActivityDetails?.id{
      
      viewModel.activityUpdate(id: "\(activityID)", state: "0")
    }
    else{
      self.navigationController?.popViewController(animated: true)
    }
    
  }
  
  @IBAction func sendBtnAction(_ sender: Any) {
    
    if let _addBathRoomRequest = addBathRoomRequest{
      
      viewModel.addBathRoomActiviy(at: _addBathRoomRequest)
      
    }else if let _request = updateBathRoomRequest, let _activityID = viewDailyActivityDetails?.id {
      
      viewModel.updateBathRommActivity(at: _request, at: _activityID)
      
    }else{
      if let activityID = viewDailyActivityDetails?.id{
        
        viewModel.activityUpdate(id: "\(activityID)", state: "1")
      }
    }
  }
}

//MARK: UITableViewDataSource,UITableViewDelegate Methods
extension BathroomDailyActivityVC: UITableViewDataSource,UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch indexPath.section {
      
    case 0 :
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedStudentsTableViewCell", for: indexPath) as! SelectedStudentsTableViewCell
      cell.selectionStyle = .none
      
      if activityId != 0 {
        cell.activityTypeView.isHidden = true
        
      }else {
        cell.activityTypeView.isHidden = false
        cell.activityTypeLabel.text = activityType.rawValue + "Activity"
      }
      
      cell.editBtn.isHidden = true
      
      if let arrayStudentName = addBathRoomRequest?.studentName {
        cell.selectedUsers = arrayStudentName
      }
      
      if let arrayStudentName = updateBathRoomRequest?.studentName {
        cell.selectedUsers = arrayStudentName
        
      }else {
        if let students = viewDailyActivityDetails?.students {
          cell.selectedUsers = students.map({$0.studentName ?? ""})
        }
      }
      cell.collectionViewHeight.constant = cell.selectedStuCollectionView.collectionViewLayout.collectionViewContentSize.height
      cell.selectedStuCollectionView.reloadData()
      return cell
      
    case 1:
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityDetailsTimeTableViewCell", for: indexPath) as! ActivityDetailsTimeTableViewCell
      cell.selectionStyle = .none
      
      cell.labelStartTime.text = "Time"
      cell.endTimeLabel.text = "Bathroom Type"
      cell.labelCategory.text = "Diaper Change"
      
      cell.endTime.text = (addBathRoomRequest?.start_time ?? "") + " to " + (addBathRoomRequest?.end_time ?? "")
      cell.labelTime.text = bathRoomType
      
      if  addBathRoomRequest?.disperChange == 1 {
        cell.labelClassRoomActivity.text = "Yes"
        
      }else {
        cell.labelClassRoomActivity.text = "No"
      }
      
      if updateBathRoomRequest == nil {
        
        if let _viewDailyActivityDetails = viewDailyActivityDetails {
          
          cell.labelTime.text = _viewDailyActivityDetails.startTime! + " to " + _viewDailyActivityDetails.endTime!
          cell.endTime.text = _viewDailyActivityDetails.bathroomTypeName
          
          if _viewDailyActivityDetails.diaperChange == 1 {
            cell.labelClassRoomActivity.text = "Yes"
            
          }else{
            cell.labelClassRoomActivity.text = "No"
          }
        }
        
      }else {
        
        cell.labelTime.text = (updateBathRoomRequest?.start_time ?? "") + " to " + (updateBathRoomRequest?.end_time ?? "")
        cell.endTime.text = bathRoomType
        
        if updateBathRoomRequest?.disperChange == 1 {
          cell.labelClassRoomActivity.text = "Yes"
          
        }else{
          cell.labelClassRoomActivity.text = "No"
        }
      }
      cell.editBtn.addTarget(self, action: #selector(editBtnAction(button:)), for: .touchUpInside)
      return cell
      
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell", for: indexPath) as! DescriptionTableViewCell
      cell.selectionStyle  = .none
      
      if let _addNapRequest = addBathRoomRequest {
        cell.descriptionLabel.text = _addNapRequest.description
      }
      
      if updateBathRoomRequest?.description != nil {
        cell.descriptionLabel.text = updateBathRoomRequest?.description
        
      }else {
        if let _viewDailyActivity = viewDailyActivityDetails {
          cell.descriptionLabel.text = _viewDailyActivity.dataDescription
        }
      }
      return cell
      
    default: return UITableViewCell()
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if section == 0 && activityId == 0 {
      let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ActivityPreviewHeaderView") as! ActivityPreviewHeaderView
      headerView.statusView.isHidden = true
      return headerView
      
    }else{
      return nil
    }
    
  }
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 && activityId == 0 {
      return 62
    }
    else {
      return 0
    }
  }
}

//MARK: DailyActivityDelegate Methods
extension BathroomDailyActivityVC: DailyActivityDelegate {
  
  func updateActivityResponse(at editActivityResponse: EditPhotoActivityEmptyResponse) {
    self.displayServerSuccess(withMessage: "Updated Bathroom Activity Successfully")
    
    for item in self.navigationController?.viewControllers ?? [] {
      if item is RootViewController {
        self.navigationController?.popToViewController(item, animated: true)
        break
      }
    }
  }
    
  func activityUpdateSuccess(activity: EditPhotoActivityEmptyResponse) {
    self.displayServerSuccess(withMessage: "Updated Successfully")
    self.delegate?.refreshDailyActivity()
    self.navigationController?.popViewController(animated: true)
  }
  
  func addDailyActivityPhotoResponse(at editActivityResponse: AddDailyAtivityPhotoResponse) {
    
    self.displayServerSuccess(withMessage: "Add Bathroom Activity Successfully")
    if let viewController = navigationController?.viewControllers.first(where: {$0 is ActivityListVC}) {
      navigationController?.popToViewController(viewController, animated: true)
    }
  }
  
  func viewDailyActivitySuccessfull(dailyActivityDetails: DailyActivityDetail) {
    viewDailyActivityDetails = dailyActivityDetails
    activityTableView.reloadData()
  }
  
  func bathRoomList(at bathRoomList: [CategoryListDatum]) {
  }
  
  func classRoomMilestoneList(at CategoryList: [CategoryListDatum]) {
  }
  
  func classRoomCategoryList(at CategoryList: [CategoryListDatum]) {
  }
  
  func editPhotEditActivityResponse(at editActivityResponse: EditPhotoActivityEmptyResponse) {
  }
  
  func getListDailyActivity(at dailyActivityList: [DailyActivity]) {
  }
  
  func failure(message: String) {
    displayServerError(withMessage: message)
  }
  
}

extension UINavigationController {
  
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
