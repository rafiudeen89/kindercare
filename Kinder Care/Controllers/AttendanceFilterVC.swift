//
//  AttendanceFilterVC.swift
//  Kinder Care
//
//  Created by CIPL0681 on 02/12/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit
import BEMCheckBox
import DropDown

protocol reportsTableViewDelegate{
  func reportAttendanceDelegate(userType: UserType, classModel: ClassModel, section: Section, fromDate: String, toDate: String, sectionArray: [Section])
}

class AttendanceFilterVC: BaseViewController {
  
    @IBOutlet weak var lblFilterByClassAndSection: UILabel!
    @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var fromDateTxtFld: CTTextField!
  @IBOutlet weak var toDateTxtFld: CTTextField!
  @IBOutlet weak var studentCheckBox: BEMCheckBox!
  @IBOutlet weak var teacherCheckBox: BEMCheckBox!
  @IBOutlet weak var adminCheckBox: BEMCheckBox!
  @IBOutlet weak var classFilterStackView: UIStackView!
  @IBOutlet weak var sectionFilterStackView: UIStackView!
  
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblSection: UILabel!
  @IBOutlet weak var lblClass: UILabel!
  @IBOutlet weak var txtClass: CTTextField!
  
  @IBOutlet weak var classAndSectionStackView: UIStackView!
  @IBOutlet weak var txtSection: CTTextField!
  @IBOutlet weak var sectionView: UIView!
  @IBOutlet weak var classView: UIView!
  @IBOutlet weak var adminStackView: UIStackView!
  
    @IBOutlet weak var applyAndCearAllStackView: UIStackView!
    var selectedClass: ClassModel!
  var selectedSection: Section!
  var delegate:reportsTableViewDelegate?
  var selectedUserType: UserType = .student
  var schoolID:Int?
  var fromDate:String?
  var toDate:String?
  var selectedDate:Date?
  var fromDateSelected: Date?
  var sectionID:Int?
  var classID:Int?
  var attendanceListArray = [Attendance]()
  let dispatchGroup = DispatchGroup()
  var classNameListArray = [ClassModel]()
  var sectionArray = [Section]()
  let window = UIApplication.shared.windows.first
  var sectionIndex : Int = 0
  var classIndex : Int = 0
  
  lazy var viewModel : AttendanceListViewModel = {
    return AttendanceListViewModel()
  }()
  
  //MARK: ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    viewModel.delegate = self
    
    if let _fromDate = fromDate,let _toDate = toDate {
      
      fromDateTxtFld.text = _fromDate.getDateAsStringWith(inFormat: "yyyy-MM-dd", outFormat: "dd-MM-yyyy")
      toDateTxtFld.text = _toDate.getDateAsStringWith(inFormat: "yyyy-MM-dd", outFormat: "dd-MM-yyyy")
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd"
      fromDateSelected = dateFormatter.date(from:_fromDate)!
    }
    
    studentCheckBox.tag = UserType.student.rawValue
    teacherCheckBox.tag = UserType.teacher.rawValue
    adminCheckBox.tag = UserType.admin.rawValue
     
    for item in [studentCheckBox,teacherCheckBox,adminCheckBox] {
      
      if item?.tag == selectedUserType.rawValue {
        
        item?.on = true
        
        
      }else {
        item?.on = false
      }
    }
    
    //    let _seletcedUserType = selectedUserType
    //
    print(selectedUserType.stringValue)
    
    if selectedUserType.stringValue == "Student" {
      
      classAndSectionStackView.isHidden = false
      classID = selectedClass.id
      txtClass.text = selectedClass.className
      
      sectionID = selectedSection.id
      txtSection.text = selectedSection.section
      
    }else {
      classAndSectionStackView.isHidden = true
    }
    
    self.classNameListArray = SharedPreferenceManager.shared.classNameListArray
    self.sectionArray = SharedPreferenceManager.shared.sectionArray
    
    configureUI()
    
  }
  
  func configureUI(){
    
    if  let userType = UserManager.shared.currentUser?.userType {
      
      if let _type = UserType(rawValue:userType ) {
        
        switch  _type {
          
        case .parent,.teacher,.all,.student,.superadmin:
          adminStackView.isHidden = false
          
        case .admin:
          adminStackView.isHidden = true
        }
      }
    }
  }
  
  @IBAction func studentCheckBoxAction(_ sender: Any) {
    teacherCheckBox.on  = false
    adminCheckBox.on = false
    studentCheckBox.on = true
    
    selectedUserType = .student
    //  classFilterStackView.isHidden = false
    //   sectionFilterStackView.isHidden = false
    //classView.isHidden = false
    // sectionView.isHidden = false
    
    classAndSectionStackView.isHidden = false
      lblFilterByClassAndSection.isHidden = false
      applyAndCearAllStackView.frame = CGRect(x: 87 , y: 435, width: 195, height: 40)
  }
  
  @IBAction func teacherCheckBoxAction(_ sender: Any) {
    studentCheckBox.on  = false
    adminCheckBox.on = false
    teacherCheckBox.on  = true
    selectedUserType = .teacher
    classFilterStackView.isHidden = true
    sectionFilterStackView.isHidden = true
    classAndSectionStackView.isHidden = true
    classView.isHidden = true
    sectionView.isHidden = true
      lblFilterByClassAndSection.isHidden = true
      applyAndCearAllStackView.frame = CGRect(x: 87 , y: 400, width: 195, height: 40)
  }
  
  @IBAction func adminCheckBoxAction(_ sender: Any) {
    
    teacherCheckBox.on  = false
    studentCheckBox.on = false
    adminCheckBox.on = true
    
    selectedUserType = .admin
    classFilterStackView.isHidden = true
    sectionFilterStackView.isHidden = true
    classAndSectionStackView.isHidden = true
    classView.isHidden = true
    sectionView.isHidden = true
      lblFilterByClassAndSection.isHidden = true
      applyAndCearAllStackView.frame = CGRect(x: 87 , y: 400, width: 195, height: 40)
    
  }
  
  func setupClass() {
    
    for item in classFilterStackView.subviews {
      item.removeFromSuperview()
    }
    
    for x in 0...classNameListArray.count - 1 {
      let item  = classNameListArray[x]
      let stack = UIStackView()
      stack.axis = .horizontal
      
      let classlabel = createLabel()
      classlabel.text = item.className
      
      let checkBox = createCheckBox()
      checkBox.addTarget(self, action: #selector(classRoomAction(sender:)), for: .valueChanged)
      checkBox.tag = x
      
      if selectedClass.id == item.id {
        checkBox.on = true
      }
      else {
        checkBox.on = false
      }
      
      stack.addArrangedSubview(classlabel)
      stack.addArrangedSubview(checkBox)
      classFilterStackView.addArrangedSubview(stack)
    }
  }
  
  func setupSection() {
    
    for item in sectionFilterStackView.subviews {
      item.removeFromSuperview()
    }
    
    for x in 0...sectionArray.count - 1{
      let item  = sectionArray[x]
      let stack = UIStackView()
      stack.axis = .horizontal
      
      let classlabel = createLabel()
      classlabel.text = item.section
      
      let checkBox = createCheckBox()
      checkBox.addTarget(self, action: #selector(sectionRoomAction(sender:)), for: .valueChanged)
      checkBox.tag = x
      
      if selectedSection.id == item.id{
        checkBox.on = true
        
      }else {
        checkBox.on = false
      }
      
      stack.addArrangedSubview(classlabel)
      stack.addArrangedSubview(checkBox)
      sectionFilterStackView.addArrangedSubview(stack)
    }
  }
  
  func createLabel() -> UILabel {
    let label = UILabel()
    label.font = UIFont.boldSystemFont(ofSize: 12)
    label.textColor = .ctDarkGray
    return label
  }
  
  func createCheckBox() -> BEMCheckBox {
    
    let checkBox = BEMCheckBox(frame: CGRect(origin: .zero, size: CGSize(width: 20, height: 20)))
    checkBox.onTintColor = UIColor.ctBlue
    checkBox.onCheckColor = UIColor.clear
    return checkBox
  }
  
  override func viewDidLayoutSubviews() {
    contentView.roundCorners([.topLeft, .topRight], radius: 20)
  }
  
  @objc func classRoomAction(sender:BEMCheckBox) {
    
    for case let item as UIStackView in classFilterStackView.subviews {
      
      for case let checkbox as BEMCheckBox in item.subviews {
        checkbox.on = false
      }
    }
    sender.on = true
    selectedClass = classNameListArray[sender.tag]
    if let schoolId = self.schoolID {
      viewModel.sectionList(school_id: schoolId, class_id: selectedClass.id ?? 0)
    }
  }
  
  @objc func sectionRoomAction(sender:BEMCheckBox) {
    
    for case let item as UIStackView in sectionFilterStackView.subviews {
      
      for case let checkbox as BEMCheckBox in item.subviews {
        checkbox.on = false
      }
    }
    sender.on = true
    selectedSection = sectionArray[sender.tag]
  }
  
  @IBAction func closeAction(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func clearAll(_ sender: Any) {
    print("clear all")
    fromDateTxtFld.text = ""
    toDateTxtFld.text = ""
    teacherCheckBox.on = false
    studentCheckBox.on = false
    adminCheckBox.on = false
  }
  
    @IBAction func apply(_ sender: Any) {
        if let fromDate = fromDate,let toDate = toDate {
          
          self.dismiss(animated: true, completion: {
            
            self.delegate?.reportAttendanceDelegate(userType: self.selectedUserType, classModel: self.selectedClass, section:self.selectedSection, fromDate: fromDate, toDate: toDate, sectionArray: self.sectionArray)
          })
          
        }else {
          displayServerError(withMessage: "Please Select Types")
        }
    }
    @IBAction func applyAction(_ sender: Any) {
    print("sghjkrs")
    if let fromDate = fromDate,let toDate = toDate {
      
//      self.dismiss(animated: true, completion: {


////        self.delegate?.reportAttendanceDelegate(userType: self.selectedUserType, classModel: self.selectedClass, section:self.selectedSection, fromDate: fromDate, toDate: toDate, sectionArray: self.sectionArray)
//      })
        if toDate.compare(fromDate) == .orderedDescending  || toDate.compare(fromDate) == .orderedSame{
                       
            self.dismiss(animated: true, completion: {
            self.delegate?.reportAttendanceDelegate(userType: self.selectedUserType, classModel: self.selectedClass, section:self.selectedSection, fromDate: fromDate, toDate: toDate, sectionArray: self.sectionArray)
            })
        }else{
           
                self.displayServerError(withMessage: "Leave from should be less  leave till")
        }
    }else {
      displayServerError(withMessage: "Please Select Types")
    }
  }
  
  func getStudentAttendanceList() {
    
    if let _classID  = self.classID, let _sectionID = self.sectionID, let _schoolID = schoolID, let toDate = toDate {
      
      attendanceListArray.removeAll()
      self.viewModel.studentAttendanceList(school_id:_schoolID, date:toDate , section_id: _sectionID, class_id: _classID)
    }
    
    dispatchGroup.notify(queue: .main, execute: {
      // MBProgressHUD.hide(for: self.window!, animated: true)
    })
  }
  
  //MARK: Show DropDown
  func showDropDown(sender : UITextField, content : [String]) {
    
    let dropDown = DropDown()
    dropDown.direction = .any
    dropDown.anchorView = sender
    dropDown.dismissMode = .automatic
    dropDown.dataSource = content
    
    dropDown.selectionAction = { (index: Int, item: String) in
      
      sender.text = item
      
      if sender == self.txtClass {
        self.classID = self.classNameListArray[index].id
        self.lblClass.text = self.txtClass.text
        self.selectedClass = self.classNameListArray[index]
      }
      
      if sender == self.txtSection {
        
        self.sectionID = self.sectionArray[index].id
        self.lblSection.text = self.txtSection.text
        self.selectedSection = self.sectionArray[index]
      }
    }
    
    dropDown.width = sender.frame.width
    dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
    dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
    
    if let visibleDropdown = DropDown.VisibleDropDown {
      visibleDropdown.dataSource = content
      
    }else {
      dropDown.show()
    }
  }
}

//MARK: UITextFieldDelegate Methods
extension AttendanceFilterVC: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if (range.location == 0 && string == " ") {
      return false
    }
    return true
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    if textField == txtClass {
      
      showDropDown(sender: textField, content: SharedPreferenceManager.shared.classNameListArray.map({$0.className}))
      return false
      
    } else if textField == txtSection {
      
      showDropDown(sender: textField, content: SharedPreferenceManager.shared.sectionArray.map({$0.section!}))
      return false
    }
            if textField == fromDateTxtFld   {
    
                let picker = self.showDateTimePicker(mode: .date, selectedDate: selectedDate)
               // toDateTxtFld.text = ""
              picker.timePicker.maximumDate = Date()
                picker.dismissBlock = { date in
                    self.selectedDate = date
                    self.fromDateSelected = date
                    self.fromDateTxtFld.text = date.asString(withFormat: "dd-MM-yyyy")
                    self.fromDate = date.asString(withFormat: "yyyy-MM-dd")
                 
                   // picker.timePicker.minimumDate = self.selectedDate
                }
                return false
            }
    
            if textField == toDateTxtFld   {
    
                let picker = self.showDateTimePicker(mode: .date, selectedDate: selectedDate)
               picker.timePicker.maximumDate = Date()
                
                picker.dismissBlock = { date in
                    self.selectedDate = date
                    
                    self.toDateTxtFld.text = date.asString(withFormat: "dd-MM-yyyy")
                    self.toDate = date.asString(withFormat: "yyyy-MM-dd")
                }
                return false
            }
    
    return true
  }
}

//MARK: AttendanceListDelegate Methods
extension AttendanceFilterVC: AttendanceListDelegate {
  
  func getAttendanceListSuccess(attendanceList: [Attendance], forUserType: Int) {
  }
  
  func updateAttendanceListSuccess() {
  }
  
  func gettingAttendanceListFailure(message: String) {
  }
  
  func getAttendanceListSuccess(attendanceList: [Attendance]) {
  }
  
  func getClassNameListSuccess(classList: [ClassModel]) {
  }
  
  func getSectionNameListSuccess(sectionList: [Section]) {
    print(sectionList)
    sectionArray = sectionList
    selectedSection = sectionArray.first!
    self.setupSection()
  }
  
  func addStudentAttendanceList() {
    
  }
  
  func failure(message: String) {
    displayServerError(withMessage: message)
  }
  
}
//extension UIScrollView {
//
//  func scrollToBottom() {
//    let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
//    setContentOffset(bottomOffset, animated: true)
//  }
//}
