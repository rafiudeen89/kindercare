//
//  MessageAttechmentViewController.swift
//  Kinder Care
//
//  Created by CIPL0590 on 7/14/20.
//  Copyright © 2020 Athiban Ragunathan. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class MessageAttechmentViewController: BaseViewController {
  
  @IBOutlet weak var messageTblView: UITableView!
  var urlStr : String = ""
  var attachmentsDetails:[MessageDetails]=[]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    titleString = "ATTACHMENTS"
    
    self.messageTblView.register(UINib(nibName: "AttachmentViewAllTableViewCell", bundle: nil), forCellReuseIdentifier: "AttachmentViewAllTableViewCell")
  }
  
  
  
  
}

extension MessageAttechmentViewController : UITableViewDelegate,UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentViewAllTableViewCell", for: indexPath) as! AttachmentViewAllTableViewCell
    
    cell.videoPlayImage.isHidden = true
    cell.attachmentImageView.isHidden = true
    cell.imageWebView.isHidden = false
    
    if let thumbImg  = attachmentsDetails.first?.webattachments {
      
      
      if let url = URL(string: thumbImg[indexPath.row].link!) {
        //print(url,"image url")
        // urlStr = url
        urlStr = "\(url)"
        print(urlStr,"image url")
        let request = URLRequest(url: url)
        
        cell.imageWebView.loadRequest(request)
      }
    }
    cell.attachmentNameLabel.text = attachmentsDetails.first?.webattachments![indexPath.row].name
    cell.attachmentDownloadButton.addTarget(self, action: #selector(videoDownloadAction(_:)), for: .touchUpInside)
    return cell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return attachmentsDetails.first?.attachment?.count ?? 0
  }
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 190
    
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    previewPhotobrowser(indexPath: indexPath)
  }
  
  @objc func videoDownloadAction(_ sender : UIButton){
    
    //self.displayServerError(withMessage: "Downloaded Successfully")
    
    if !Reachability.isConnectedToNetwork() {
      self.displayServerError(withMessage: "Please Check Your Internet Connection")
      return
    }
    
    if urlStr.contains(".pdf") {
      // print("exists")
      downloadFile()
      
    }else {
      
      let documentDirectory = FileManager.default.urls(for: .documentDirectory,
                                                          in: .userDomainMask)[0]
      let imageName = documentDirectory.appendingPathComponent("myImage.png")
      
      //  let urlString = "https://raw.githubusercontent.com/programmingwithswift/HowToSaveFileFromUrl/master/testFile.png"
      
      // 1
      if let imageUrl = URL(string: urlStr) {
        // 2
        URLSession.shared.downloadTask(with: imageUrl) { (tempFileUrl, response, error) in
          
          // 3
          if let imageTempFileUrl = tempFileUrl {
            do {
              // Write to file
              let imageData = try Data(contentsOf: imageTempFileUrl)
              try imageData.write(to: imageName)
              
              checkSavedImage()
            } catch {
              print("Error")
            }
          }
        }.resume()
      }
      
      func checkSavedImage() {
        do {
          let savedImageData = try Data(contentsOf: imageName)
          
          if let savedImage = UIImage(data: savedImageData) {
            //savedImage
          }
        } catch {
          print("Cannot read saved")
        }
      }
    }
    
    //downloadFile()
  }
  func downloadFile(){
    let url = urlStr
    let fileName = "MyFile"
    
    let character: Character = "e"
    savePdf(urlString: url, fileName: fileName)
    
  }
  
  func savePdf(urlString: String, fileName:String) {
    
    DispatchQueue.main.async {
      
      let urlString = urlString
      let url = URL(string: urlString)
      let fileName = String((url!.lastPathComponent)) as NSString
      // Create destination URL
      let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
      
      let pdfNameFromUrl = "YourAppName-\(fileName).pdf"
      let destinationFileUrl = documentsUrl.appendingPathComponent("\(pdfNameFromUrl)")
      
      let fileURL = URL(string: urlString)
      let sessionConfig = URLSessionConfiguration.default
      let session = URLSession(configuration: sessionConfig)
      let request = URLRequest(url: fileURL!)
      let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
        
        if let _ = tempLocalUrl, error == nil {
          // Success
          if let statusCode = (response as? HTTPURLResponse)?.statusCode {
            
            DispatchQueue.main.async {
              print("Successfully downloaded. Status code: \(statusCode)")
              self.displayServerError(withMessage: "Successfully downloaded.")
            }
          }
//          do {
//            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
//            do {
//              //Show UIActivityViewController to save the downloaded file
//              let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
//              for indexx in 0..<contents.count {
//                if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
//                  let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
//                  self.present(activityViewController, animated: true, completion: nil)
//                }
//              }
//            }
//            catch (let err) {
//              print("error: \(err)")
//            }
//          } catch (let writeError) {
//            print("Error creating a file \(destinationFileUrl) : \(writeError)")
//          }
          
        } else {
          print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
        }
      }
      task.resume()
      //      let pdfData = try? Data.init(contentsOf: url!)
      //      let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
      //      let pdfNameFromUrl = "YourAppName-\(fileName).pdf"
      //      let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
      //      do {
      //        try pdfData?.write(to: actualPath, options: .atomic)
      //        print("pdf successfully saved!")
      //        //file is downloaded in app data container, I can find file from x code > devices > MyApp > download Container >This container has the file
      //      } catch {
      //        print("Pdf could not be saved")
      //      }
      
      
    }
  }
  func previewPhotobrowser(indexPath:IndexPath) {
    
    var images = [SKPhoto]()
    
    let activityAttachment = attachmentsDetails
    
    for arrayImagesPath in activityAttachment.map({$0.webattachments}) {
      
      if let imageFilePath = arrayImagesPath?.map({$0.link}){
        let photo  = SKPhoto.photoWithImageURL(imageFilePath[indexPath.row] as! String)
        photo.shouldCachePhotoURLImage = false
        images.append(photo)
      }
    }
    
    let browser = SKPhotoBrowser(photos: images)
    browser.initializePageIndex(0)
    present(browser, animated: true, completion: {})
    
  }
  
}
