//
//  MedicineDailyActivityVC.swift
//  Kinder Care
//
//  Created by Ragavi Rajendran on 29/11/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit

class MedicineDailyActivityVC: BaseViewController {
  
  @IBOutlet var activityTableView: UITableView!{
    
    didSet {
      
      self.activityTableView.register(UINib(nibName: "ActivitySelectedStudentTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivitySelectedStudentTableViewCell")
      self.activityTableView.register(UINib(nibName: "ActivityPreviewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ActivityPreviewHeaderView")
      self.activityTableView.register(UINib(nibName: "SelectedStudentsTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectedStudentsTableViewCell")
      self.activityTableView.register(UINib(nibName: "ActivityDetailsTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivityDetailsTimeTableViewCell")
      self.activityTableView.register(UINib(nibName: "DescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionTableViewCell")
      
    }
  }
  @IBOutlet var statusLbl: UILabel!
  @IBOutlet var sendBtn: UIButton!
  @IBOutlet var cancelBtn: UIButton!
  @IBOutlet weak var sendCancelView: CTView!
  
  var state: Int?
  
  var activityType: ActivityType!
  var viewDailyActivityDetails: DailyActivityDetail?
  var delegate: refreshDailyActivityDelegate?
  
  lazy var viewModel: DailyActivityViewModel = {
    return DailyActivityViewModel()
  }()
  
  var addMediniceRequest: AddMedicineActivityRequest?
  var updateMediniceRequest: UpdateDailyActivityRequest?
  public var activityId: Int = 0
  
  //MARK: ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    //  viewModel.delegate = self
    //  configureUI()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    print("viewWillAppear")
    viewModel.delegate = self
    configureUI()
  }
  override func viewDidAppear(_ animated: Bool) {
    print("viewDidAppear")
    //viewModel.delegate = self
    // configureUI()
  }
  override func viewDidDisappear(_ animated: Bool) {
    print("viewDidDisappear")
    // viewModel.delegate = self
    //configureUI()
  }
  func configureUI() {
    
    if viewDailyActivityDetails != nil {
      titleString = "PREVIEW"
      
    }else {
      if activityId != 0 {
        titleString = (activityType.rawValue).uppercased()
        viewModel.viewDailyActivity(activity_id: activityId)
      }else {
        titleString = "PREVIEW"
      }
    }
    
    if  let userType = UserManager.shared.currentUser?.userType {
      if let _type = UserType(rawValue:userType ) {
        switch  _type  {
          
        case .admin :
          
          if activityId != 0 {
            
            let editTopBtn = UIButton(frame: CGRect(x: self.view.frame.width - (16 + 65), y: 15 + safeAreaHeight, width: 70, height: 30))
            editTopBtn.setTitle("Edit", for: .normal)
            editTopBtn.setImage(UIImage(named: "EditWhite"), for: .normal)
            editTopBtn.backgroundColor = UIColor.clear
            editTopBtn.tag = 3
            editTopBtn.addTarget(self, action: #selector(editBtnAction(button:)), for: .touchUpInside)
            self.view.addSubview(editTopBtn)
            
            sendBtn.setTitle("APPROVE", for: .normal)
            cancelBtn.setTitle("REJECT", for: .normal)
            
          }else{
            sendBtn.setTitle("Send", for: .normal)
            cancelBtn.setTitle("Cancel", for: .normal)
          }
          if state == 1 {
            sendCancelView.isHidden = true
          }
          else if state == 0 {
            sendCancelView.isHidden = true
          }else{
            sendCancelView.isHidden = false
          }
          
        case.teacher:
          if activityId != 0 {
            
            sendBtn.isHidden = true
            cancelBtn.isHidden = true
          }
          else {
            sendBtn.isHidden = false
            cancelBtn.isHidden = false
          }
          sendBtn.setTitle("Send", for: .normal)
          cancelBtn.setTitle("Reject", for: .normal)
          if state == 1 {
            sendCancelView.isHidden = true
          }
          else if state == 0 {
            sendCancelView.isHidden = true
          }else{
            sendCancelView.isHidden = false
          }
          
        default :
          
          cancelBtn.setTitle("Cancel", for: .normal)
          break
        }
      }
    }
  }
  
  //MARK: Button Action
  @IBAction func cancelBtnAction(_ sender: Any) {
    if let activityID = viewDailyActivityDetails?.id{
      viewModel.activityUpdate(id: "\(activityID)", state: "0")
      
    }else{
      self.navigationController?.popViewController(animated: true)
    }
  }
  
  @IBAction func sendBtnAction(_ sender: Any) {
    
    if let _addBathRoomRequest = addMediniceRequest{
      viewModel.addMediniceActivity(at: _addBathRoomRequest)
      
    }else if let _request = updateMediniceRequest, let _activityID = viewDailyActivityDetails?.id {
      viewModel.updateMediniceActivity(at: _request, at: _activityID)
      
    }else{
      if let activityID = viewDailyActivityDetails?.id {
        viewModel.activityUpdate(id: "\(activityID)", state: "1")
      }
    }
    
  }
  
  @objc func editBtnAction(button : UIButton) {
    
    if button.tag == 1 {
      let vc = UIStoryboard.AddActivityStoryboard().instantiateViewController(withIdentifier:"StudentListVC") as! StudentListVC
      vc.modalPresentationStyle = .overCurrentContext
      self.navigationController?.present(vc, animated: true, completion: nil)
      
    }else if button.tag == 3 {
      let story = UIStoryboard(name: "AddActivity", bundle: nil)
      let nextVC = story.instantiateViewController(withIdentifier: "SelectStudentVC") as! SelectStudentVC
      nextVC.activityType = activityType
      nextVC.viewDailyActivityDetails = viewDailyActivityDetails
      self.navigationController?.pushViewController(nextVC, animated: true)
      
    }else {
      let vc = UIStoryboard.AddActivityStoryboard().instantiateViewController(withIdentifier:"AddActivityVC") as! AddActivityVC
      vc.activityType = activityType
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
}

//MARK: UITableViewDataSource, UITableViewDelegate Methods
extension MedicineDailyActivityVC: UITableViewDataSource, UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch indexPath.section {
      
    case 0 :
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedStudentsTableViewCell", for: indexPath) as! SelectedStudentsTableViewCell
      cell.selectionStyle = .none
      cell.editBtn.isHidden = true
      
      if activityId != 0 {
        cell.activityTypeView.isHidden = true
        
      }else{
        cell.activityTypeView.isHidden = false
        cell.activityTypeLabel.text = activityType.rawValue + "Activity"
      }
      
      if let arrayStudentName = addMediniceRequest?.studentName {
        cell.selectedUsers = arrayStudentName
      }
      
      if let arrayStudentName = updateMediniceRequest?.studentName {
        cell.selectedUsers = arrayStudentName
        
      }else {
        if let students = viewDailyActivityDetails?.students {
          cell.selectedUsers = students.map({$0.studentName ?? ""})
        }
      }
      
      cell.collectionViewHeight.constant = cell.selectedStuCollectionView.collectionViewLayout.collectionViewContentSize.height
      cell.selectedStuCollectionView.reloadData()
      return cell
      
    case 1:
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityDetailsTimeTableViewCell", for: indexPath) as! ActivityDetailsTimeTableViewCell
      cell.selectionStyle = .none
      
      if addMediniceRequest != nil {
        
        if let stime = addMediniceRequest?.start_time {
          cell.labelStartTime.text = "Time"
          cell.labelTime.text = stime
        }
        
        if let temperture = addMediniceRequest?.temperature {
          
          cell.endTimeLabel.text = "Sanitizer & Temperture"
          
          if addMediniceRequest?.sanitizer == 0 {
            cell.endTime.text = "No" + " & " + temperture
            
          }else{
            cell.endTime.text = "Yes" + " & " + temperture
          }
        }
        cell.labelCategory.text  = "Sanitizer"
        cell.labelCategory.isHidden = false
        
      }else {
        
        if updateMediniceRequest == nil {
          
          if let _viewDailyActivity = viewDailyActivityDetails {
            
            cell.labelStartTime.text  = "Time"
            cell.labelTime.text = _viewDailyActivity.startTime
            
            cell.endTimeLabel.text = "Sanitizer"
            
            if _viewDailyActivity.sanitizer == 0 {
              cell.endTime.text = "No"
            }else{
              cell.endTime.text = "Yes"
            }
            cell.labelCategory.text  = "Temperture"
            cell.labelClassRoomActivity.text = "\(_viewDailyActivity.temp)"
          }
          
        }else {
          
          cell.labelStartTime.text  = "Time"
          cell.labelTime.text = updateMediniceRequest?.start_time
          
          cell.endTimeLabel.text = "Sanitizer"
          
          if addMediniceRequest?.sanitizer == 0 {
            cell.endTime.text = "No"
            
          }else {
            cell.endTime.text = "Yes"
          }
          cell.labelCategory.text  = "Temperture"
          cell.labelClassRoomActivity.text = updateMediniceRequest?.temperature
        }
      }
      /* if let _viewDailyActivity = viewDailyActivityDetails {
       
       cell.labelStartTime.text  = "Time"
       cell.labelTime.text = _viewDailyActivity.startTime
       
       cell.endTimeLabel.text = "Temperture"
       
       if addMediniceRequest?.sanitizer == 0 {
       cell.endTime.text = "No"
       }else{
       cell.endTime.text = "Yes"
       }
       
       cell.labelCategory.text  = "Sanitizer"
       cell.labelClassRoomActivity.text = "\(_viewDailyActivity.temp)"
       
       }else {
       
       if let stime = addMediniceRequest?.start_time {
       cell.labelStartTime.text = "Time"
       cell.labelTime.text = stime
       }
       
       if let temperture = addMediniceRequest?.temperature {
       cell.endTimeLabel.text = "Sanitizer & Temperture"
       
       if addMediniceRequest?.sanitizer == 0 {
       cell.endTime.text = "No" + " & " + temperture
       
       }else{
       cell.endTime.text = "Yes" + " & " + temperture
       }
       }
       cell.labelCategory.text  = "Sanitizer"
       cell.labelCategory.isHidden = false
       }*/
      
      cell.editBtn.addTarget(self, action: #selector(editBtnAction(button:)), for: .touchUpInside)
      return cell
      
    case 2:
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell", for: indexPath) as! DescriptionTableViewCell
      cell.selectionStyle  = .none
      
      if let _viewDailyActivity = viewDailyActivityDetails {
        cell.descriptionLabel.text = _viewDailyActivity.dataDescription
        
      }else{
        cell.descriptionLabel.text = addMediniceRequest?.description
      }
      return cell
      
    default:return UITableViewCell()
      
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if section == 0 && activityId == 0 {
      let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ActivityPreviewHeaderView") as! ActivityPreviewHeaderView
      headerView.statusView.isHidden = true
      return headerView
      
    }else{
      return nil
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    if section == 0 && activityId == 0 {
      return 62
      
    }else {
      return 0
    }
  }
}

//MARK: DailyActivityDelegate Methods
extension MedicineDailyActivityVC : DailyActivityDelegate {
  
  func updateActivityResponse(at editActivityResponse: EditPhotoActivityEmptyResponse) {
    self.displayServerSuccess(withMessage: "Updated Medicine Activity Details Successfully")
    
    for item in self.navigationController?.viewControllers ?? [] {
      if item is RootViewController {
        self.navigationController?.popToViewController(item, animated: true)
        break
      }
    }
    
  }
  
  func activityUpdateSuccess(activity: EditPhotoActivityEmptyResponse) {
    self.displayServerSuccess(withMessage: "Updated Successfully")
    self.delegate?.refreshDailyActivity()
    self.navigationController?.popViewController(animated: true)
  }
  
  func addDailyActivityPhotoResponse(at editActivityResponse: AddDailyAtivityPhotoResponse) {
    self.displayServerSuccess(withMessage: "Add Medicine Activity Successfully")
    
    if let viewController = navigationController?.viewControllers.first(where: {$0 is ActivityListVC}) {
      navigationController?.popToViewController(viewController, animated: true)
    }
  }
  
  func viewDailyActivitySuccessfull(dailyActivityDetails: DailyActivityDetail) {
    viewDailyActivityDetails = dailyActivityDetails
    activityTableView.reloadData()
  }
  
  func failure(message: String) {
    displayServerError(withMessage: message)
  }
  
  func bathRoomList(at bathRoomList: [CategoryListDatum]) {
  }
  
  func classRoomMilestoneList(at CategoryList: [CategoryListDatum]) {
  }
  
  func classRoomCategoryList(at CategoryList: [CategoryListDatum]) {
  }
  
  func editPhotEditActivityResponse(at editActivityResponse: EditPhotoActivityEmptyResponse) {
  }
  
  func getListDailyActivity(at dailyActivityList: [DailyActivity]) {
  }
  
  
}
