//
//  PaymentListVC.swift
//  Kinder Care
//
//  Created by CIPL0023 on 19/11/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit

class PaymentListVC: BaseViewController {

     //MARK:- Initialization
     @IBOutlet weak var tblPaymentList: UITableView!
     @IBOutlet weak var childDropDown: ChildDropDown!
    
     var paymentType = "invoice"
     var selectedIndexPath:IndexPath?
     var childNameArray = [ChildName]()
      var childNameID:Int?
    var paymentListArray = [PaymentListData]()
    var  noDataLabel: UILabel = UILabel()
    lazy var viewModel : familyInformationViewModel = {
        return familyInformationViewModel()
    }()
    
    
          //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        topBarHeight = 100
        titleString = "PAYMENT"
        
        if let childName = UserManager.shared.childNameList {
            childNameArray = childName
            self.childNameID = childNameArray.map({$0.id}).first
        }
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tblPaymentList.bounds.size.width, height: self.tblPaymentList.bounds.size.height))
               
               noDataLabel.textColor = .lightGray
               noDataLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
               noDataLabel.textAlignment = .center
        self.tblPaymentList.separatorStyle = .none
             self.tblPaymentList.backgroundView = noDataLabel
        tblPaymentList.delegate = self
        tblPaymentList.dataSource = self
        tblPaymentList.tableFooterView = UIView()
        tblPaymentList.register(UINib(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
        
        self.view.bringSubviewToFront(childDropDown)
        print(self.childNameID, UserManager.shared.currentUser?.school_id, "chils id and school id")
        if let childId = self.childNameID,let schoolID = UserManager.shared.currentUser?.school_id {
            self.viewModel.paymentList(student_id: "\(childId)", school_id: "\(schoolID)")

        }
        
        self.childNameDropDown()
        

    }
    
    func childNameDropDown(){
        
        childDropDown.titleArray = childNameArray.map({$0.name})
        childDropDown.subtitleArray = childNameArray.map({$0.className + " , " + $0.section + " Section"})
        
        if childNameArray.count > 1 {
            
            
            
            childDropDown.selectionAction = { (index : Int) in
                print(index)
                self.childNameID = self.childNameArray[index].id
                
                if let childId = self.childNameID,let schoolID = UserManager.shared.currentUser?.school_id {
                    self.viewModel.paymentList(student_id: "\(childId)", school_id: "\(schoolID)")
                    
                }
                
            }
        }
        else{
            childDropDown.isUserInteractionEnabled = false
        }
        childDropDown.addChildAction = { (sender : UIButton) in
            let vc = UIStoryboard.FamilyInformationStoryboard().instantiateViewController(withIdentifier:"AddChildVC") as! AddChildVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    //MARK:- Button Action
    @IBAction func selectPayment(_ sender: CTSegmentControl) {
        self.selectedIndexPath = nil
        if sender.selectedSegmentIndex == 0{
            paymentType = "invoice"
            noDataLabel.text = ""
        }else{
            paymentType = "transaction"
            noDataLabel.text = "No Data Found!"
        }
        self.tblPaymentList.reloadData()
    }
    
    @objc func termAction(sender: UIButton){
        
        if let cell = sender.superview?.superview?.superview?.superview?.superview?.superview?.superview?.superview as? PaymentCell{
            
            if let indexPath = tblPaymentList.indexPath(for: cell){
                
                let nextVC = UIStoryboard.paymentStoryboard().instantiateViewController(withIdentifier: "InvoiceDetailsVC") as! InvoiceDetailsVC
                nextVC.paymentListArray = paymentListArray
               nextVC.indexPath = indexPath.row
                nextVC.term = "Term 1"
               // nextVC.titleString = "PAYMENT"
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        }
        
    }
    
    @objc func term2Action(sender: UIButton){
        
        if let cell = sender.superview?.superview?.superview?.superview?.superview?.superview?.superview?.superview as? PaymentCell{
            
            if let indexPath = tblPaymentList.indexPath(for: cell){
                
                let nextVC = UIStoryboard.paymentStoryboard().instantiateViewController(withIdentifier: "PaymentDetailsVC") as! PaymentDetailsVC
                
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        }
    }
    
    @objc func term3Action(sender: UIButton){
        
        if let cell = sender.superview?.superview?.superview?.superview?.superview?.superview?.superview?.superview as? PaymentCell{
            
            if let indexPath = tblPaymentList.indexPath(for: cell){
                
                let nextVC = UIStoryboard.paymentStoryboard().instantiateViewController(withIdentifier: "InvoiceDetailsVC") as! InvoiceDetailsVC
                nextVC.paymentListArray = paymentListArray
               nextVC.indexPath = indexPath.row
                nextVC.term = "Term 2"
               // nextVC.titleString = "PAYMENT"
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        }
        
    }
}


//MARK:- TableView Delegates
extension PaymentListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if paymentType == "transaction"{
             return 0
        }
        else{
            return paymentListArray.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PaymentCell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
        cell.selectionStyle = .none
        
        if paymentType == "transaction"{
            cell.btnDropdown.isHidden  = true
            
            let main_string = "Paid  ₹ 3000"
            let string_to_color = "₹ 3000"
            
            let range = (main_string as NSString).range(of: string_to_color)
            let attributedString = NSMutableAttributedString.init(string: main_string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.ctBlue, range: range)
            attributedString.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0, weight: .regular)], range: NSMakeRange(0, attributedString.length))
            
            cell.labelPaid.attributedText = attributedString
        }else{
            cell.btnDropdown.isHidden  = true
            
            cell.dateLbl.text = paymentListArray[indexPath.row].date
            let pendingValue =   paymentListArray[indexPath.row].pending
            var main_string = ""
            if pendingValue == "0.00" {
                 main_string = "Paid \(paymentListArray[indexPath.row].totalAmount)"
            }else{
                main_string = "Pending \(paymentListArray[indexPath.row].totalAmount)"
            }
         // let main_string = "Paid \(paymentListArray[indexPath.row].totalAmount)"
            let string_to_color = "\(String(describing: paymentListArray[indexPath.row].pending))"
            
            let range = (main_string as NSString).range(of: string_to_color)
            let attributedString = NSMutableAttributedString.init(string: main_string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.ctYellow, range: range)
            attributedString.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0, weight: .regular)], range: NSMakeRange(0, attributedString.length))
            cell.labelPaid.attributedText = attributedString
          //  cell.labelTotalAmount.text = "Total Amount" + " " +  "\(paymentListArray[indexPath.row].totalAmount)"
            cell.labelTotalAmount.text = "\(paymentListArray[indexPath.row].totalAmount)"
            cell.labelTermName.text =  "\(paymentListArray[indexPath.row].invoiceName)"
            cell.labelId.text = "#\(paymentListArray[indexPath.row].studentInvoiceID)"
            var paymentTypevalue = paymentListArray[indexPath.row].paymentType
            if paymentTypevalue == 1 {
                cell.labelTermType.text = "Term Type Full"
            }else{
                cell.labelTermType.text = "Term Type Partially"
                cell.labelPendingAmount.text = "\(paymentListArray[indexPath.row].totalAmount).0"
                cell.btnDropdown.isHidden = false
            }
          //  cell.labelTermType.text = "Term Type Full"
        }
        
        cell.setPaymentStatusView(type: self.paymentType)
        
        cell.btnDropdown.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
        cell.term1.addTarget(self, action: #selector(termAction), for: .touchUpInside)
        cell.term2.addTarget(self, action: #selector(term2Action), for: .touchUpInside)
        cell.term3.addTarget(self, action: #selector(term3Action), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        if paymentType == "transaction"{
            let nextVC = UIStoryboard.paymentStoryboard().instantiateViewController(withIdentifier: "PaymentDetailsVC") as! PaymentDetailsVC
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
        else {
            var paymentTypevalue = paymentListArray[indexPath.row].paymentType
            if paymentTypevalue == 1 {
                let vc = UIStoryboard.paymentStoryboard().instantiateViewController(withIdentifier: "InvoiceDetailsVC") as! InvoiceDetailsVC
                  vc.paymentListArray = paymentListArray
                 vc.indexPath = indexPath.row
            //    vc.labelStatus.text = "Paid"
             //   vc.labelStatus.backgroundColor = .green
                
                self.navigationController?.pushViewController(vc, animated: true)
            }

            
                
            
        }
    }
    
    @objc func dropdownAction(sender: UIButton){
        
        if let cell = sender.superview?.superview?.superview?.superview?.superview as? PaymentCell{
            if let indexPath = tblPaymentList.indexPath(for: cell){
                
                //expand and CollapseCell
                let cell = self.tblPaymentList.cellForRow(at: indexPath)  as! PaymentCell
                
                if self.selectedIndexPath == indexPath{
                    self.selectedIndexPath = nil
                    cell.vwPaymentStatus.isHidden = true
                    cell.vwPaymentStatus.layoutIfNeeded()
                }else{
                    self.selectedIndexPath = indexPath
                    cell.vwPaymentStatus.isHidden = false
                    cell.vwPaymentStatus.layoutIfNeeded()
            }
                
            tblPaymentList.beginUpdates()
            tblPaymentList.endUpdates()
        }
      }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   /* func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        
        if  paymentType == "transaction"{
            let nextVC = UIStoryboard.paymentStoryboard().instantiateViewController(withIdentifier: "PaymentDetailsVC") as! PaymentDetailsVC
            self.navigationController?.pushViewController(nextVC, animated: true)
        }else{
            
            //expand and CollapseCell
            let cell = self.tblPaymentList.cellForRow(at: indexPath)  as! PaymentCell
            
            if self.selectedIndexPath == indexPath{
                self.selectedIndexPath = nil
//                UIView.animate(withDuration: 1.0, delay: 0.0, options: [.transitionCurlDown], animations: {
//                    cell.vwPaymentStatus.isHidden = true
//                    cell.vwPaymentStatus.layoutIfNeeded()
//                }, completion: nil)
                
                cell.vwPaymentStatus.isHidden = true
                cell.vwPaymentStatus.layoutIfNeeded()
                
            }else{
                self.selectedIndexPath = indexPath
//                UIView.animate(withDuration: 1.0, delay: 0.0, options: [.transitionCurlUp], animations: {
//                    cell.vwPaymentStatus.isHidden = false
//                    cell.vwPaymentStatus.layoutIfNeeded()
//                }, completion: nil)
                
                cell.vwPaymentStatus.isHidden = false
                cell.vwPaymentStatus.layoutIfNeeded()
            }
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }*/
    
    /*func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 1) {
            cell.alpha = 1.0
        }
    }*/
}


extension PaymentListVC:familyInformationDelegate{
    func paymentUpdateSuccess() {
        
    }
    
    func paymentList(paymentList: [PaymentListData]) {
        self.paymentListArray = paymentList
        if paymentListArray.isEmpty{
                        // alertMsg(title: "Alert", message: "You Don't have any Active Quotes!" )
                        
                        noDataLabel.text = "NO DATA FOUND !!!!"
                        
                        
                    }
        self.tblPaymentList.reloadData()
    }
    
    func familyDetailsList(familyDetails: FamilyInformationData?) {
        
    }
    
    func editPickupSuccess() {
        
    }
    
    func failure(message: String) {
        
    }
    
    
    
    
}

