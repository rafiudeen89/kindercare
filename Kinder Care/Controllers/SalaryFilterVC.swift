//
//  SalaryFilterVC.swift
//  Kinder Care
//
//  Created by CIPL0681 on 03/12/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit
import DropDown
import BEMCheckBox


protocol salaryFilterTableViewDelegate {
  
  func salaryFilterTableView(selectUserType: UserType, fromDate: String, toDate: String)
}

class SalaryFilterVC: BaseViewController {
  
  @IBOutlet weak var labelAdmin: UILabel!
  @IBOutlet weak var fromDateTxtFld: CTTextField!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var toDateTxtFld: CTTextField!
  @IBOutlet weak var teacherCheckBox: BEMCheckBox!
  @IBOutlet weak var adminCheckBox: BEMCheckBox!
  
  var delegate: salaryFilterTableViewDelegate?
  var userTypeId: Int?
  var fromDate: String?
  var toDate: String?
  var selectedDate: Date?
  var fromDateSelected: Date?
  let userTypeName = UserManager.shared.currentUser?.userTypeName
  let userName = UserManager.shared.currentUser?.name
  var selectedUserType: UserType = .admin
  
  //MARK: ViewController LifeStyle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if userTypeName == "super_admin" {
      adminCheckBox.isHidden = false
      labelAdmin.isHidden = false
      
    }else{
      adminCheckBox.isHidden = true
      labelAdmin.isHidden = true
    }
    
    if let _fromDate = fromDate,let _toDate = toDate {
      fromDateTxtFld.text = _fromDate.getDateAsStringWith(inFormat: "yyyy-MM-dd", outFormat: "dd-MM-yyyy")
      toDateTxtFld.text = _toDate.getDateAsStringWith(inFormat: "yyyy-MM-dd", outFormat: "dd-MM-yyyy")
    }
    
    //    teacherCheckBox.on = true
    //    userTypeId = UserType.teacher.rawValue
    
    teacherCheckBox.tag = UserType.teacher.rawValue
    adminCheckBox.tag = UserType.admin.rawValue
    
    for item in [teacherCheckBox,adminCheckBox] {
      
      if item?.tag == selectedUserType.rawValue {
        
        item?.on = true
        
      }else {
        item?.on = false
      }
    }
  }
  
  //MARK: ShowDropDown
  func showDropDown(sender : UITextField, content : [String]) {
    
    let dropDown = DropDown()
    dropDown.direction = .any
    dropDown.anchorView = sender
    dropDown.dismissMode = .automatic
    dropDown.dataSource = content
    
    dropDown.selectionAction = { (index: Int, item: String) in
      sender.text = item
      print(item)
    }
    
    dropDown.width = sender.frame.width
    dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
    dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
    
    if let visibleDropdown = DropDown.VisibleDropDown {
      visibleDropdown.dataSource = content
      
    }else {
      dropDown.show()
    }
  }
  
  override func viewDidLayoutSubviews() {
    contentView.roundCorners([.topLeft, .topRight], radius: 20)
  }
  
  //MARK: UIButton Action Methods
  @IBAction func clearAll(_ sender: Any) {
    fromDateTxtFld.text = ""
    toDateTxtFld.text = ""
    adminCheckBox.on = false
    teacherCheckBox.on = false
  }
  
  @IBAction func closeAction(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func cancelAction(_ sender: UIButton) {
    
  }
  @IBAction func applyAction(_ sender: UIButton) {
    
    if let fromDate = fromDate,let toDate = toDate {
      
      self.dismiss(animated: true, completion: nil)
      
      self.delegate?.salaryFilterTableView(selectUserType: selectedUserType, fromDate: fromDate, toDate: toDate)
      
    }else {
      displayServerError(withMessage: "Please Select Types")
    }
  }
  
  @IBAction func teacherCheckAction(_ sender: Any) {
    //userTypeId = UserTypeTitle.teacher.id
    adminCheckBox.on = false
    teacherCheckBox.on = true
    selectedUserType = .teacher
    print(selectedUserType.rawValue)
  }
  
  @IBAction func adminCheckbox(_ sender: Any) {
    // userTypeId = UserTypeTitle.admin.id
    teacherCheckBox.on = false
    adminCheckBox.on = true
    selectedUserType = .admin
    print(selectedUserType.rawValue)
  }
}

//MARK: UITextFieldDelegate Methods
extension SalaryFilterVC: UITextFieldDelegate {
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    if textField == fromDateTxtFld {
      
      let picker = self.showDateTimePicker(mode: .date, selectedDate: selectedDate)
      toDateTxtFld.text = ""
      
      picker.dismissBlock = { date in
        self.selectedDate = date
        self.fromDateSelected = date
        self.fromDateTxtFld.text = date.asString(withFormat: "dd-MM-yyyy")
        self.fromDate = date.asString(withFormat: "yyyy-MM-dd")
      }
      return false
    }
    
    if textField == toDateTxtFld {
      
      let picker = self.showDateTimePicker(mode: .date, selectedDate: selectedDate)
      picker.timePicker.minimumDate = fromDateSelected
      picker.dismissBlock = { date in
        self.selectedDate = date
        self.toDateTxtFld.text = date.asString(withFormat: "dd-MM-yyyy")
        self.toDate = date.asString(withFormat: "yyyy-MM-dd")
      }
      return false
    }
    return true
  }
  
}
