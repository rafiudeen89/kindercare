//
//  RootViewController.swift
//  Kinder Care
//
//  Created by CIPL0681 on 11/11/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit
import RESideMenu

class RootViewController: RESideMenu {
  
    let userType = UserManager.shared.currentUser?.userType
  override func viewDidLoad() {
    super.viewDidLoad()
   //  let user =  UserDefaults.standard.set("", forKey: "Parent")
      let parentData = UserDefaults.standard.string(forKey: "Parent") ?? ""
    // Do any additional setup after loading the view.
      if userType == 5  && parentData.isEmpty {
          let alert = UIAlertController(title: "Info", message: "Welcome to parent.", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
          self.present(alert, animated: true, completion: nil)
           UserDefaults.standard.set("true", forKey: "Parent")
      }
  }
  
  override func awakeFromNib() {
    
    if  let userType = UserManager.shared.currentUser?.userType {
      if let _type = UserType(rawValue:userType ) {
        
        if  _type.rawValue == 5 {
          self.contentViewController = UIStoryboard.dashboardStoryboard().instantiateViewController(withIdentifier: "DashboardVC")
        }
          
        else{
          self.contentViewController = UIStoryboard.commonDashboardStoryboard().instantiateViewController(withIdentifier: "CommonDashboardVC")
        }
      }
    }
    
    self.leftMenuViewController = UIStoryboard.onBoardingStoryboard().instantiateViewController(withIdentifier: "leftMenuViewController")
    
    backgroundImage = UIImage(named: "BG")
    
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destination.
   // Pass the selected object to the new view controller.
   }
   */
  
}
