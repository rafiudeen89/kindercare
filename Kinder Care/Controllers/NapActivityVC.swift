//
//  NapActivityVC.swift
//  Kinder Care
//
//  Created by Ragavi Rajendran on 29/11/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit

class NapActivityVC: BaseViewController {
  
  @IBOutlet var activityTableView: UITableView! {
    didSet {
      activityTableView.register(UINib(nibName: "ActivitySelectedStudentTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivitySelectedStudentTableViewCell")
      activityTableView.register(UINib(nibName: "ActivityPreviewHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ActivityPreviewHeaderView")
      activityTableView.register(UINib(nibName: "SelectedStudentsTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectedStudentsTableViewCell")
      activityTableView.register(UINib(nibName: "ActivityDetailsTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivityDetailsTimeTableViewCell")
      activityTableView.register(UINib(nibName: "DescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionTableViewCell")
    }
  }
  
  @IBOutlet var statusLbl: UILabel!
  
  @IBOutlet var sendBtn: UIButton!
  @IBOutlet var cancelBtn: UIButton!
  @IBOutlet var bottomCancelSendView: CTView!
  
  var viewDailyActivityDetails: DailyActivityDetail?
  var addNapRequest: AddNapActivityRequest?
  
  var delegate: refreshDailyActivityDelegate?
  
  lazy var viewModel : DailyActivityViewModel =  {
    return DailyActivityViewModel()
  }()
  
  public var updateNapActivityRequest: UpdateDailyActivityRequest?
  public var activityId: Int = 0
  public var state: Int?
  public var activityType: ActivityType!
  
  //MARK: ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    viewModel.delegate = self
    configureUI()
  }
  
  //MARK: configureUI
  func configureUI() {
    
    if viewDailyActivityDetails != nil {
      titleString = "PREVIEW"
      
    }else {
      if activityId != 0 {
        titleString = (activityType.rawValue).uppercased()
        viewModel.viewDailyActivity(activity_id: activityId)
      }else {
        titleString = "PREVIEW"
      }
    }
    
    if  let userType = UserManager.shared.currentUser?.userType {
      
      if let _type = UserType(rawValue:userType ) {
        switch  _type  {
          
        case .admin :
          
          if activityId != 0 {
            
            let editTopBtn = UIButton(frame: CGRect(x: self.view.frame.width - (16 + 65), y: 15 + safeAreaHeight, width: 70, height: 30))
            editTopBtn.setTitle("Edit", for: .normal)
            editTopBtn.setImage(UIImage(named: "EditWhite"), for: .normal)
            editTopBtn.backgroundColor = UIColor.clear
            editTopBtn.tag = 3
            editTopBtn.addTarget(self, action: #selector(editBtnAction(button:)), for: .touchUpInside)
            self.view.addSubview(editTopBtn)
            
            sendBtn.setTitle("Send", for: .normal)
            cancelBtn.setTitle("Reject", for: .normal)
            
            if state == 1 {
              bottomCancelSendView.isHidden = true
            }
            else if state == 0 {
              bottomCancelSendView.isHidden = true
            }else{
              bottomCancelSendView.isHidden = false
            }
            
          }else{
            sendBtn.setTitle("Send", for: .normal)
            cancelBtn.setTitle("Cancel", for: .normal)
            
            if state == 1 {
              bottomCancelSendView.isHidden = true
            }
            else if state == 0 {
              bottomCancelSendView.isHidden = true
            }else{
              bottomCancelSendView.isHidden = false
            }
            
          }
          
        case.teacher:
          
          if activityId == 0 {
            bottomCancelSendView.isHidden = true
          }
          else {
            bottomCancelSendView.isHidden = false
            
          }
          
          sendBtn.setTitle("Send", for: .normal)
          cancelBtn.setTitle("Cancel", for: .normal)
          
          if state == 1 {
            bottomCancelSendView.isHidden = true
          }
          else if state == 0 {
            bottomCancelSendView.isHidden = true
          }else{
            bottomCancelSendView.isHidden = false
          }
          
        default :
          sendBtn.setTitle("Send", for: .normal)
          bottomCancelSendView.isHidden = false
          cancelBtn.setTitle("Cancel", for: .normal)
          
          if state == 1 {
            bottomCancelSendView.isHidden = true
          }
          else if state == 0 {
            bottomCancelSendView.isHidden = true
          }else{
            bottomCancelSendView.isHidden = false
          }
          
          break
        }
      }
    }
  }
  
  @objc func editBtnAction(button: UIButton) {
    
    if button.tag == 1 {
      
      let story = UIStoryboard(name: "AddActivity", bundle: nil)
      let nextVC = story.instantiateViewController(withIdentifier: "SelectStudentVC") as! SelectStudentVC
      nextVC.activityType = activityType
      self.navigationController?.pushViewController(nextVC, animated: true)
      
    }else if button.tag == 3 {
      
      let story = UIStoryboard(name: "AddActivity", bundle: nil)
      let nextVC = story.instantiateViewController(withIdentifier: "SelectStudentVC") as! SelectStudentVC
      nextVC.activityType = activityType
      nextVC.viewDailyActivityDetails = viewDailyActivityDetails
      self.navigationController?.pushViewController(nextVC, animated: true)
      
      
    }else{
      
      let vc = UIStoryboard.AddActivityStoryboard().instantiateViewController(withIdentifier:"AddActivityVC") as! AddActivityVC
      vc.activityType = activityType
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  
  //MARK: Button Action
  @IBAction func cancelBtnAction(_ sender: Any) {
    
    if let activityID = viewDailyActivityDetails?.id {
      viewModel.activityUpdate(id: "\(activityID)", state: "0")
    }else {
      self.navigationController?.popViewController(animated: true)
    }
    
  }
  
  @IBAction func sendBtnAction(_ sender: Any) {
    
    if let _addNapRequest = addNapRequest{
      
      viewModel.addNapActivity(at: _addNapRequest)
      
    }else if let _addNapRequest = updateNapActivityRequest, let _activityID = viewDailyActivityDetails?.id {
      
      viewModel.updateNapActivity(at: _addNapRequest, at: _activityID)
      
    }else{
      
      if let activityID = viewDailyActivityDetails?.id {
        
        if UserManager.shared.currentUser?.userTypeName == "admin" {
          viewModel.activityUpdate(id: "\(activityID)", state: "1")
        }
        
      }else {
     
     self.navigationController?.popViewController(animated: true)
      }
    }
  }
}

//MARK: UITableViewDataSource,UITableViewDelegate Methods
extension NapActivityVC: UITableViewDataSource,UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch indexPath.section {
      
    case 0 :
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedStudentsTableViewCell", for: indexPath) as! SelectedStudentsTableViewCell
      cell.activityTypeLabel.isHidden = true
      cell.selectionStyle = .none
      cell.editBtn.isHidden = true
      
      if activityId != 0 {
        cell.activityTypeView.isHidden = true
        
      }else {
        cell.activityTypeView.isHidden = false
        cell.activityTypeLabel.text = activityType.rawValue + "Activity"
      }
      
      if let arrayStudentName = addNapRequest?.studentName {
        cell.selectedUsers = arrayStudentName
      }
      
      if let arrayStudentName = updateNapActivityRequest?.studentName {
        cell.selectedUsers = arrayStudentName
        
      }else {
        if let students = viewDailyActivityDetails?.students {
          cell.selectedUsers = students.map({$0.studentName ?? ""})
        }
      }
      
      cell.collectionViewHeight.constant = cell.selectedStuCollectionView.collectionViewLayout.collectionViewContentSize.height
      cell.selectedStuCollectionView.reloadData()
      
      return cell
      
    case 1 :
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityDetailsTimeTableViewCell", for: indexPath) as! ActivityDetailsTimeTableViewCell
      cell.selectionStyle = .none
      
      if let _viewDailyActivity = viewDailyActivityDetails {
        cell.labelTime.text = _viewDailyActivity.startTime
        cell.endTime.text = _viewDailyActivity.endTime
        
      }else {
        
        if let _addNapRequest = addNapRequest {
          cell.labelTime.text = _addNapRequest.start_time
          cell.endTime.text = _addNapRequest.end_time
          
        }else{
          cell.labelTime.text = updateNapActivityRequest?.start_time
          cell.endTime.text = updateNapActivityRequest?.end_time
        }
      }
      
      cell.catergoryStackView.isHidden = true
      cell.editBtn.addTarget(self, action: #selector(editBtnAction(button:)), for: .touchUpInside)
      
      return cell
      
    case 2 :
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell", for: indexPath) as! DescriptionTableViewCell
      cell.selectionStyle = .none
      
      if let _addNapRequest = addNapRequest {
        cell.descriptionLabel.text = _addNapRequest.description
      }
      
      if updateNapActivityRequest?.description != nil {
        cell.descriptionLabel.text = updateNapActivityRequest?.description
        
      }else {
        if let _viewDailyActivity = viewDailyActivityDetails {
          cell.descriptionLabel.text = _viewDailyActivity.dataDescription
        }
      }
      
      return cell
      
    default : return UITableViewCell()
      
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if section == 0 && activityId == 0 {
      let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ActivityPreviewHeaderView") as! ActivityPreviewHeaderView
      headerView.statusView.isHidden = true
      return headerView
      
    }else{
      return nil
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    if section == 0 && activityId == 0 {
      return 62
      
    }else {
      return 0
    }
  }
  
}

//MARK: DailyActivityDelegate Methods
extension NapActivityVC: DailyActivityDelegate {
  
  func updateActivityResponse(at editActivityResponse: EditPhotoActivityEmptyResponse) {
    
    self.displayServerSuccess(withMessage: "Updated Nap Activity Successfully")
    
    for item in self.navigationController?.viewControllers ?? [] {
      if item is RootViewController {
        self.navigationController?.popToViewController(item, animated: true)
        break
      }
    }

  }
  
  func activityUpdateSuccess(activity: EditPhotoActivityEmptyResponse) {
    self.displayServerSuccess(withMessage: "Updated Successfully")
      
    self.delegate?.refreshDailyActivity()
    self.navigationController?.popViewController(animated: true)
  }
  
  func addDailyActivityPhotoResponse(at editActivityResponse: AddDailyAtivityPhotoResponse) {
    
    self.displayServerSuccess(withMessage: "Added Nap Activity Successfully")
    if let viewController = navigationController?.viewControllers.first(where: {$0 is ActivityListVC}) {
      navigationController?.popToViewController(viewController, animated: true)
        
    }
      
  }
  
  func viewDailyActivitySuccessfull(dailyActivityDetails: DailyActivityDetail) {
    viewDailyActivityDetails = dailyActivityDetails
    activityTableView.reloadData()
  }
  
  func bathRoomList(at bathRoomList: [CategoryListDatum]) {
  }
  
  func classRoomMilestoneList(at CategoryList: [CategoryListDatum]) {
  }
  
  func classRoomCategoryList(at CategoryList: [CategoryListDatum]) {
  }
  
  func editPhotEditActivityResponse(at editActivityResponse: EditPhotoActivityEmptyResponse) {
  }
  
  func getListDailyActivity(at dailyActivityList: [DailyActivity]) {
  }
  
  func failure(message: String) {
    displayServerError(withMessage: message)
  }
}
