//
//  FamilyInformationServiceManager.swift
//  Kinder Care
//
//  Created by CIPL0590 on 4/8/20.
//  Copyright © 2020 Athiban Ragunathan. All rights reserved.
//

import Foundation

enum FamilyInformationServiceManager {
    
    case familyDetail(student_id:Int)
    case editPickup(pickupID:String,pickup_person:String,relationship:String,pickup_contact:String,_method:String)
    case paymentList(studentID:String,schoolID:String)
    
    case paymentUpdate(studentId:Int, student_invoice_id : String, transaction_id:String,type:Int,received:Double,name:String,terms:Int,fees_type:Int,email:String)
    var scheme: String {
        switch self {
        case .familyDetail(_),.editPickup(_),.paymentList(_), .paymentUpdate(_): return API.scheme
     
    
        }
    }
    var host: String {
        switch self {
        case .familyDetail(_),.editPickup(_),.paymentList(_): return API.baseURL
        case .paymentUpdate(_) : return API.baseURL
        }
    }
    
    var path: String {
        switch self {
            
        case .familyDetail(_) : return API.path + "/parent/familyinformation/filter"
            
        case .editPickup(let pickupID,_,_,_,_): return API.path + "/parent/pickupperson/update/" + pickupID
            
        case .paymentList(let studentID,_) : return API.path +
            "/payment/student/\(studentID)/list"
        
            
        case .paymentUpdate(let studentId,_,_,_,_,_,_,_,_) : return API.path + "/payment/student/" + "\(studentId)" + "/pay"
            }
       
    }
    
    var method: String {
        switch self {
        case .familyDetail(_),.editPickup(_),.paymentUpdate(_): return "POST"
        case .paymentList(_):return "GET"
        }
    }
    
    var port:Int{
        switch self {
        case .familyDetail(_),.editPickup(_),.paymentList(_),.paymentUpdate(_): return API.port
            
        }
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .familyDetail(_),.editPickup(_),.paymentUpdate(_):
            return nil
            
        case .paymentList(let studentID,let schoolID):
            return [URLQueryItem(name:"school_id",value:schoolID)]
            
        }
    }
    
    var body: Data? {
        switch self {
            
        case .familyDetail(_),.editPickup(_),.paymentList(_),.paymentUpdate(_): return nil
            
        }
    }
    var headerFields: [String : String]
    {
        switch self {
        case .familyDetail(_),.editPickup(_),.paymentList(_), .paymentUpdate(_):
            return ["Accept":"application/json","API_VERSION" : "1.0", "DEVICE_TYPE" : "iOS"]
            
        }
    }
    
    var formDataParameters : [String : Any]? {
        
        switch self {
        case .familyDetail(let student_id) :
            
            let parameters = ["student_id" : student_id] as [String : Any]
            return parameters
            
        case .editPickup(_,let pickup_person,let relationship,let pickup_contact,let _method) :
            
            let parameters = ["pickup_person":pickup_person,
                              "relationship":relationship,
                              "pickup_contact":pickup_contact,
                              "_method":_method]
            return parameters
            
        
        case .paymentList(_,_):
        //let parameters = ["student_id" : studentID,"school_id":schoolID] as [String : Any]
        return nil
        
        case .paymentUpdate(_, let student_invoice_id, let transaction_id, let type, let received, let name, let terms, let fees_type, let email) :
            let parameters = [
                "student_invoice_id": student_invoice_id,
                "transaction_id": transaction_id,
                "type":type,
                "received" : received,
                "name": name,
                "terms": terms,
                "fees_type":fees_type,
                "email": email
            ] as [String : Any]
            return parameters
        }
    }
}

