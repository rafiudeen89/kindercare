//
//  CTButton1.swift
//  Kinder Care
//
//  Created by CIPL0957 on 01/12/21.
//  Copyright © 2021 Athiban Ragunathan. All rights reserved.
//

import UIKit

class ButtonIconRight: UIButton {
    override func imageRect(forContentRect contentRect:CGRect) -> CGRect {
        var imageFrame = super.imageRect(forContentRect: contentRect)
        imageFrame.origin.x = super.titleRect(forContentRect: contentRect).maxX - imageFrame.width
        return imageFrame
    }

    override func titleRect(forContentRect contentRect:CGRect) -> CGRect {
        var titleFrame = super.titleRect(forContentRect: contentRect)
        if (self.currentImage != nil) {
            titleFrame.origin.x = super.imageRect(forContentRect: contentRect).minX
        }
        return titleFrame
    }
}
//extension ButtonIconRight {
//    func addRightIcon(image: UIImage) {
//        let imageView = UIImageView(image: image)
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//
//        addSubview(imageView)
//
//        let length = CGFloat(15)
//        titleEdgeInsets.right += length
//
//        NSLayoutConstraint.activate([
//            imageView.leadingAnchor.constraint(equalTo: self.titleLabel!.trailingAnchor, constant: 100),
//            imageView.centerYAnchor.constraint(equalTo: self.titleLabel!.centerYAnchor, constant: 0),
//            imageView.widthAnchor.constraint(equalToConstant: length),
//            imageView.heightAnchor.constraint(equalToConstant: length)
//        ])
//    }
//}
