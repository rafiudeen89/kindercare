//
//  AssignedTableViewCell.swift
//  Kinder Care
//
//  Created by CIPL0590 on 7/1/20.
//  Copyright © 2020 Athiban Ragunathan. All rights reserved.
//

import UIKit

class AssignedTableViewCell: UITableViewCell {

    @IBOutlet weak var assignedView: UIView!
    @IBOutlet weak var teacherLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var teacher1StLbl: UILabel!
    @IBOutlet weak var email2Lbl: UILabel!
    @IBOutlet weak var contact3rdLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
        assignedView.layer.borderColor = UIColor.lightGray.cgColor
        assignedView.layer.borderWidth = 0.2
        assignedView.layer.cornerRadius = 5
        
        // drop shadow
        assignedView.layer.shadowColor = UIColor.lightGray.cgColor
        assignedView.layer.shadowOpacity = 0.5
       // assignedView.layer.shadowRadius = 3.0
        assignedView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
