//
//  EmptyActivityTableViewCell.swift
//  Kinder Care
//
//  Created by CIPL0957 on 02/11/21.
//  Copyright © 2021 Athiban Ragunathan. All rights reserved.
//

import UIKit

class EmptyActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblActivity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.layer.borderWidth = 0.2
        mainView.layer.cornerRadius = 5
        
        // drop shadow
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 0.5
       // assignedView.layer.shadowRadius = 3.0
        mainView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
