//
//  PaymentCell.swift
//  Kinder Care
//
//  Created by CIPL0023 on 19/11/19.
//  Copyright © 2019 Athiban Ragunathan. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {

    @IBOutlet weak var vwPaymentStatus: CTView!
    
    @IBOutlet weak var labelId: UILabel!
    @IBOutlet weak var labelPaid: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var term1: UIButton!
    @IBOutlet weak var term2: UIButton!
    @IBOutlet weak var term3: UIButton!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var labelPendingAmount: UILabel!
    @IBOutlet weak var labelTermName: UILabel!
    @IBOutlet weak var labelTermType: UILabel!
    @IBOutlet weak var labelTotalAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPaymentStatusView(type: String){
        if type == "invoice"{
            vwPaymentStatus.isHidden = true
        }else{
            vwPaymentStatus.isHidden = true
        }
    }
    
}
